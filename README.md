# Anarchie

![Sample of the font Anarchie](./sample.png)

## Download

You can download the latest build of the font on the [Releases page](https://gitlab.com/renardrebelle/anarchie-font/-/releases).
